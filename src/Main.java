public class Main {
    public static void main(String[] args) {
        Book b0 = new Book("Harry Potter and the Philosopher's Stone", "J.K.Rowling", "Fantasy", 1997);
        Book b1 = new Book("Harry Potter and the Chamber of Secrets", "J.K.Rowling", "Fantasy", 1998);
        Book b2 = new Book("Tekvin", "Arif Ergin", "Roman", 2018);
        Book b3 = new Book("The Demon-Haunted World", "Carl Sagan", "Biography", 1997);
        Book b4 = new Book("Atomic Habits", "James Clear", "Self-help", 2018);
        Book b5 = new Book("1984", "George Orwell", "Social science fiction", 1949);

        Library historicalLibrary = new Library();

        // add books
        historicalLibrary.addBook(b0);
        historicalLibrary.addBook(b1);
        historicalLibrary.addBook(b2);
        historicalLibrary.addBook(b3);
        historicalLibrary.addBook(b4);
        historicalLibrary.addBook(b5);

        // remove some
        historicalLibrary.removeBook(b2);
        historicalLibrary.removeBook(b4);

        // search methods
        historicalLibrary.searchBook(b2);
        System.out.println(historicalLibrary.searchByAuthor("J.K.Rowling"));
        System.out.println(historicalLibrary.searchByTitle("Secret"));
        System.out.println(historicalLibrary.searchByGenre("Biography"));

        // sort and display books
        historicalLibrary.displayBooks("title");
        historicalLibrary.displayBooks("gen");
        historicalLibrary.displayBooks("genre");
        historicalLibrary.displayBooks("publication year");

        System.out.println("Average publication year of books in library: " +
                historicalLibrary.getAveragePubYear());
    }
}