import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class Library {
    private ArrayList<Book> books = new ArrayList<>();

    void addBook(Book book) {
        if (books.add(book)) {
            System.out.println("Book named " + book.getTitle() + " added successfully.");
            return;
        }

        System.out.println("There was a problem while adding book.");
    }

    void removeBook(Book book) {
        if (books.remove(book)) {
            System.out.println("Book named " + book.getTitle() + " removed successfully.");
            return;
        }

        System.out.println("There was a problem while removing the book. Check it again.");
    }

    void searchBook(Book book) {
        if (books.contains(book)) {
            System.out.println("Book found successfully.");
            return;
        }

        System.out.println("Book is not in the library.");
    }

    LinkedList<Book> searchByTitle(String title) {
        return books.stream()
                .filter(book -> book.getTitle().equals(title))
                .collect(Collectors.toCollection(LinkedList::new));
    }

    LinkedList<Book> searchByAuthor(String author) {
        return books.stream()
                .filter(book -> book.getAuthor().equals(author))
                .collect(Collectors.toCollection(LinkedList::new));
    }

    LinkedList<Book> searchByGenre(String genre) {
        return books.stream()
                .filter(book -> book.getGenre().equals(genre))
                .collect(Collectors.toCollection(LinkedList::new));
    }

    void displayBooks(String sortBy) {
        if (sortBy.equalsIgnoreCase("title")) {
            Comparator<Book> titleComparator = Comparator.comparing(Book::getTitle);
            books = books.stream()
                    .sorted(titleComparator)
                    .collect(Collectors.toCollection(ArrayList::new));
        } else if (sortBy.equalsIgnoreCase("author")) {
            Comparator<Book> authorComparator = Comparator.comparing(Book::getAuthor);
            books = books.stream()
                    .sorted(authorComparator)
                    .collect(Collectors.toCollection(ArrayList::new));
        } else if (sortBy.equalsIgnoreCase("publication year")) {
            Comparator<Book> pubYearComparator = Comparator.comparingInt(Book::getPubYear);
            books = books.stream()
                    .sorted(pubYearComparator)
                    .collect(Collectors.toCollection(ArrayList::new));
        } else {
            System.out.println("Please enter one of the below sorting criteria:");
            System.out.println("- Title");
            System.out.println("- Author");
            System.out.println("- Publication Year");
            return;
        }

        System.out.println("Books sorted by " + sortBy.toUpperCase());
        System.out.println("-----------------------------------------");
        for (Book b : books) {
            System.out.println("\t" + b);
        }
        System.out.println("-----------------------------------------");
        System.out.println("Total: " + this.getBookCounts());
    }

    long getAveragePubYear() {
        return Math.round(books.stream()
                .mapToInt(Book::getPubYear)
                .average()
                .orElse(0.0));
    }

    long getBookCounts() {
        return books.size();
    }
}
